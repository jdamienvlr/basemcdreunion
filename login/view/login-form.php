<html>
<head>
<title>Authentification</title>
<link href="./view/css/style.css" rel="stylesheet" type="text/css" />
</head>
<body>


<style>
body {
  background-image: url('../images/mcdo.jpg');
  background-repeat: no-repeat;
  background-attachment: fixed;
  background-size: cover;
}
</style> 

    <div>
        <form action="login-action.php" method="post" id="frmLogin" onSubmit="return validate();">
            <div class="demo-table">

                <div class="form-head">Authentification</div>
                <?php 
                if(isset($_SESSION["errorMessage"])) {
                ?>
                <div class="error-message"><?php  echo $_SESSION["errorMessage"]; ?></div>
                <?php 
                unset($_SESSION["errorMessage"]);
                } 
                ?>
                <div class="field-column">
                    <div>
                        <label for="username">Nom d'utilisateur</label><span id="user_info" class="error-info"></span>
                    </div>
                    <div>
                        <input name="username_comptes" id="username_comptes" type="text"
                            class="demo-input-box">
                    </div>
                </div>
                <div class="field-column">
                    <div>
                        <label for="password">Mot de passe</label><span id="password_info" class="error-info"></span>
                    </div>
                    <div>
                        <input name="password_comptes" id="password_comptes" type="password"
                            class="demo-input-box">
                    </div><br/>
                    <center>
                    <div>
                    <a href="" onclick='forgotmdp()'>Mot de passe oubliée ? </a>
                </div></center><br/>
                <div class=field-column>
                    <div>
                        <input type="submit" name="login" value="Se Connecter"
                        class="btnLogin"><br/><br/>
                        <input onclick="location.href='../login_admin/index_login.php';"  value="                         Mode Admin"
                        class="btnLogin"><br/><br/>
                        <input onclick="location.href='../Home';"  value="                             Acceuil"
                        class="btnLogin"><br/><br/>
                    </div>
                </div>
            </div>
        </form>
    </div>



    <script>
        function forgotmdp(){

alert(" En cas d'oubli, vous pouvez utiliser Outlook disponible sur ce Pc afin de contacter l'administrateur en précisant votre nom et prénom à l'adresse suivante : rgalap@mcd-reunion.com .  ")

        }
    function validate() {
        var $valid = true;
        document.getElementById("user_info").innerHTML = "";
        document.getElementById("password_info").innerHTML = "";
        
        var userName = document.getElementById("username_comptes").value;
        var password = document.getElementById("password_comptes").value;
        if(userName == "") 
        {
            document.getElementById("user_info").innerHTML = "Obligatoire";
        	$valid = false;
        }
        if(password == "") 
        {
        	document.getElementById("password_info").innerHTML = "Obligatoire";
            $valid = false;
        }
        return $valid;
    }
    </script>
</body>
</html>