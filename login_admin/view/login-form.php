<html>
<head>
<title>ADMINISTRATOR</title>
<link href="./view/css/style.css" rel="stylesheet" type="text/css" />
</head>
<body>

<style>
body {
  background-image: url('../images/mcdo.jpg');
  background-repeat: no-repeat;
  background-attachment: fixed;
  background-size: cover;
}
</style> 

    <div>
        <form action="login-action.php" method="post" id="frmLogin" onSubmit="return validate();">
            <div class="demo-table">

                <div class="form-head">INTERFACE ADMIN</div><br>
                <?php 
                if(isset($_SESSION["errorMessage"])) {
                ?>
                <div class="error-message"><?php  echo $_SESSION["errorMessage"]; ?></div>
                <?php 
                unset($_SESSION["errorMessage"]);
                } 
                ?>
                <div class="field-column">
                    <div>
                        <label for="username">Nom Admin</label><span id="user_info" class="error-info"></span>
                    </div>
                    <div>
                        <input name="username_admin" id="username_admin" type="text"
                            class="demo-input-box">
                    </div>
                </div>
                <div class="field-column">
                    <div>
                        <label for="password">Mot de passe Admin</label><span id="password_info" class="error-info"></span>
                    </div>
                    <div>
                        <input name="password_admin" id="password_admin" type="password"
                            class="demo-input-box">
                    </div>
                </div>
                <div class=field-column>
                    <div>
                        <input type="submit" name="login" value="Se Connecter"
                        class="btnLogin"><br/><br/>
                        <input onclick="location.href='../login/index_login.php';"  value="                             Retour"
                        class="btnLogin"><br/><br/>
                        <!-- <input onclick="location.href='subscribe.php';"  value="                            Inscription"
                        class="btnLogin"> -->
                    </div>
                </div>
            </div>
        </form>
    </div>



    <script>
    function validate() {
        var $valid = true;
        document.getElementById("user_info").innerHTML = "";
        document.getElementById("password_info").innerHTML = "";
        
        var userName = document.getElementById("username_admin").value;
        var password = document.getElementById("password_admin").value;
        if(userName == "") 
        {
            document.getElementById("user_info").innerHTML = "Obligatoire";
        	$valid = false;
        }
        if(password == "") 
        {
        	document.getElementById("password_info").innerHTML = "Obligatoire";
            $valid = false;
        }
        return $valid;
    }
    </script>
</body>
</html>