<!DOCTYPE html>
<html lang="en">
<head>
	<title>INSCRIPTION</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->	
	<link rel="icon" type="image/png" href="../Font_Subscribe/images/icons/favicon.ico"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="../Font_Subscribe/vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="../Font_Subscribe/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="../Font_Subscribe/fonts/iconic/css/material-design-iconic-font.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="../Font_Subscribe/vendor/animate/animate.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="../Font_Subscribe/vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="../Font_Subscribe/vendor/animsition/css/animsition.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="../Font_Subscribe/vendor/select2/select2.min.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="../Font_Subscribe/vendor/daterangepicker/daterangepicker.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="../Font_Subscribe/css/util.css">
	<link rel="stylesheet" type="text/css" href="../Font_Subscribe/css/main.css">
<!--===============================================================================================-->
</head>
<body>
    <?php 


     ?>
	
	<div class="limiter">
		<div class="container-login100" style="background-image: url('../images/mcdo.jpg');">
			<div class="wrap-login100">
				<form class="login100-form validate-form" id="form_register" method="POST">
					<center>
						<img  src="../images/logo.png" alt="Netvisionlogo" width="150px" height="120px">
					</center>
					</span>

					<span class="login100-form-title p-b-34 p-t-27">
						S'inscrire
                    </span>
                    
                    <div class="wrap-input100 validate-input" data-validate = "Entrer un nom">
						<input class="input100" type="text" name="nom_comptes" placeholder="Entrer un nom">
						<span class="focus-input100" data-placeholder="&#xf207;"></span>
					</div>

					<div class="wrap-input100 validate-input" data-validate = "Entrer un prenom">
						<input class="input100" type="text" name="prenom_comptes" placeholder="Entrer un prenom">
						<span class="focus-input100" data-placeholder="&#xf207;"></span>
					</div>

					<div class="wrap-input100 validate-input" data-validate="Entrer un username">
						<input class="input100" type="text" name="username_comptes" placeholder="Entrer un username">
						<span class="focus-input100" data-placeholder="&#xf207;"></span>
                    </div>
                    
                    <div class="wrap-input100 validate-input" data-validate="Entrer un mot de passe">
						<input class="input100" type="password" name="password_comptes" placeholder="Entrer un mot de passe">
						<span class="focus-input100" data-placeholder="&#xf191;"></span>
                    </div>
                    
                    <div class="wrap-input100 validate-input" data-validate="Confirmer mot de passe">
						<input class="input100" type="password" placeholder="Confirmer mot de passe">
						<span class="focus-input100" data-placeholder="&#xf191;"></span>
                    </div>
                    

                    <div class="wrap-input100 validate-input" data-validate = "Entrer un adresse">
						<input class="input100" type="text" name="adresse_comptes" placeholder="Entrer un adresse valide">
						<span class="focus-input100" data-placeholder="&#xf207;"></span>
					</div>

					<div class="wrap-input100 validate-input" data-validate="Entrer un numéro de tel valide">
						<input class="input100" type="text" name="telephone_comptes" placeholder="Entrer un numéro de tel valide">
						<span class="focus-input100" data-placeholder="&#xf207;"></span>
                    </div>



					
					
					

					<div class="container-login100-form-btn">
						<input class="login100-form-btn" type="submit" name="submit_register"/>
						</div>
						<br>
						<div class="container-login100-form-btn">
						<input class="login100-form-btn"  onclick="location.href='../login_admin/index_login.php';"  value="   Revenir à l'acceuil Admin"/> 
						</div>
						
						
					<div class="text-center p-t-90">
						<a class="txt1" href="#">
							Mot de passe oublié?
						</a>
					</div>

						<div class="text-center p-t-90">
						<a class="txt1">
							Déjà un compte? Se connecter :
						</a>

					</div><br>
						<div class="container-login100-form-btn">
					<a class="login100-form-btn"  value="S'inscrire" href='index_login.php'>Se Connecter</a>
					</div>


					

					
				</form>
			</div>
		</div>
    </div>
    


  

	<div id="dropDownSelect1"></div>
	
<!--===============================================================================================-->
	<script src="../Font_Subscribe/vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
	<script src="../Font_Subscribe/vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
	<script src="../Font_Subscribe/vendor/bootstrap/js/popper.js"></script>
	<script src="../Font_Subscribe/vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script src="../Font_Subscribe/vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
	<script src="../Font_Subscribe/vendor/daterangepicker/moment.min.js"></script>
	<script src="../Font_Subscribe/vendor/daterangepicker/daterangepicker.js"></script>
<!--===============================================================================================-->
	<script src="../Font_Subscribe/vendor/countdowntime/countdowntime.js"></script>
<!--===============================================================================================-->
	<script src="../Font_Subscribe/js/main.js"></script>

</body>
</html>