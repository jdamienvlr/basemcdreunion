<?php
include('database connection.php');
include('function.php');
$query = '';
$output = array();
$query .= "SELECT * FROM comptes ";
if(isset($_POST["search"]["value"]))
{
	$query .= 'WHERE id_comptes LIKE "%'.$_POST["search"]["value"].'%" ';
	$query .= 'OR nom_comptes LIKE "%'.$_POST["search"]["value"].'%" ';
	$query .= 'OR prenom_comptes LIKE "%'.$_POST["search"]["value"].'%" ';
	$query .= 'OR username_comptes LIKE "%'.$_POST["search"]["value"].'%" ';
	$query .= 'OR password_comptes LIKE "%'.$_POST["search"]["value"].'%" ';
	$query .= 'OR adresse_comptes LIKE "%'.$_POST["search"]["value"].'%" ';
	$query .= 'OR telephone_comptes LIKE "%'.$_POST["search"]["value"].'%" ';
	$query .= 'OR session_restos LIKE "%'.$_POST["search"]["value"].'%" ';

}

if(isset($_POST["order"]))
{
	$query .= 'ORDER BY '.$_POST['order']['0']['column'].' '.$_POST['order']['0']['dir'].' ';
}
else
{
	$query .= 'ORDER BY id_comptes ASC ';
}

if($_POST["length"] != -1)
{
	$query .= 'LIMIT ' . $_POST['start'] . ', ' . $_POST['length'];
}
$statement = $connection->prepare($query);
$statement->execute();
$result = $statement->fetchAll();
$data = array();
$filtered_rows = $statement->rowCount();
foreach($result as $row)
{
	$sub_array = array();
	
	$sub_array[] = $row["id_comptes"];
	$sub_array[] = $row["nom_comptes"];
	$sub_array[] = $row["prenom_comptes"];
	$sub_array[] = $row["username_comptes"];
	$sub_array[] = $row["password_comptes"];
	$sub_array[] = $row["adresse_comptes"];
	$sub_array[] = $row["telephone_comptes"];
	$sub_array[] = $row["session_restos"];
	$sub_array[] = '<button type="button" name="update" id_comptes="'.$row["id_comptes"].'" class="btn btn-primary btn-sm update"><i class="glyphicon glyphicon-pencil">&nbsp;</i>Edit</button></button>';
	$sub_array[] = '<button type="button" name="delete" id_comptes="'.$row["id_comptes"].'" class="btn btn-danger btn-sm delete">Delete</button>';
	$data[] = $sub_array;
}
$output = array(
	"draw"				=>	intval($_POST["draw"]),
	"recordsTotal"		=> 	$filtered_rows,
	"recordsFiltered"	=>	get_total_all_records(),
	"data"				=>	$data
);
echo json_encode($output);
?>