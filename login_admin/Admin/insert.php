<?php
include('database connection.php');
include('function.php');
if(isset($_POST["operation"]))
{
	if($_POST["operation"] == "Add")
	{
		$statement = $connection->prepare("
			INSERT INTO comptes (nom_comptes,prenom_comptes,username_comptes,password_comptes,adresse_comptes,telephone_comptes,session_restos) VALUES (:nom_comptes, :prenom_comptes, :username_comptes, :password_comptes, :adresse_comptes, :telephone_comptes, :session_restos)");
		$result = $statement->execute(
			array(
				':nom_comptes'	=>	$_POST["nom_comptes"],
				':prenom_comptes'	=>	$_POST["prenom_comptes"],
				':username_comptes'	=>	$_POST["username_comptes"],
				':password_comptes'	=>	hash('sha256', $salt.$_POST['password_comptes']),
				':adresse_comptes'	=>	$_POST["adresse_comptes"],
				':telephone_comptes'	=>	$_POST["telephone_comptes"],
				':session_restos'	=>	$_POST["session_restos"]
			)
		);
	}
	if($_POST["operation"] == "Edit")
	{
		$statement = $connection->prepare(
			"UPDATE comptes
			SET nom_comptes = :nom_comptes, prenom_comptes = :prenom_comptes, username_comptes = :username_comptes, password_comptes = :password_comptes, adresse_comptes = :adresse_comptes, telephone_comptes = :telephone_comptes, session_restos = :session_restos WHERE id_comptes = :id_comptes");
		$result = $statement->execute(
			array(
				':nom_comptes'	=>	$_POST["nom_comptes"],
				':prenom_comptes'	=>	$_POST["prenom_comptes"],
				':username_comptes'	=>	$_POST["username_comptes"],
				':password_comptes'	=>	hash('sha256', $salt.$_POST['password_comptes']),
				':adresse_comptes'	=>	$_POST["adresse_comptes"],
				':telephone_comptes'	=>	$_POST["telephone_comptes"],
				':session_restos'	=>	$_POST["session_restos"],
				':id_comptes'			=>	$_POST["id_comptes"]
			)
		);
	}
}

?>