<!doctype html>
<head>
      <!--Author : JDamien VLR-->

<!-- Début Head -->
    <title>INTERFACE ADMIN : GESTION DES UTILISATEURS</title>

    <link rel="stylesheet" type="text/css" href="styles.css">
    
	<!-- bootstrap Lib -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <script src="js/jquery.min.js"></script>

    <!-- datatable lib -->
    <link rel="stylesheet" href="css/jquery.dataTables.min.css">
    <script src="js/jquery-1.12.4.js"></script>
    <script src="js/jquery.dataTables.min.js"></script>
    
    <script src="js/bootstrap.min.js"></script>

   
</head>

<!-- FIN Head -->

<body>



<!-- Style : Arrière Plan -->
<style>
body {
color:black;
background-color:white;
background-image:url(mcdonalds.jpg);
background-repeat:no-repeat;
}
</style>

   

   <div class="content"> 


<!-- Sous Titre -->
    <h1>INTERFACE ADMIN : GESTION UTILISATEURS</h1>   
    
    

<!-- Tableau contenant les données utilisateur (mot de passe hashé en SHA256) -->
                <table id="course_table" class="table table-striped">  
                    <thead bgcolor="#6cd8dc">
                        <tr class="table-primary">
                           <th width="30%">ID</th>
                           <th width="50%">Nom</th>  
                           <th width="30%">Prénom</th>
                           <th width="30%">Nom Utilisateur</th>
                           <th width="50%">Mot de passe</th>  
                           <th width="30%">Adresse</th>
                           <th width="30%">N° Téléphone</th>
                           <th width="30%">N° Session Restos</th>
                           <th scope="col" width="5%">Mise à jour</th>
                           <th scope="col" width="5%">Supprimer</th>
                        </tr>
                    </thead>
                </table>
            </br>
                <div align="right">
                    <button type="button" id="add_button" data-toggle="modal" data-target="#userModal" class="btn btn-success btn-lg">Ajouter un Utilisateur</button>
                </div>
   </div>               
 </body>
 </html>

<!-- AJOUT UTILISAEUR -->
<div id="userModal" class="modal fade">
    <div class="modal-dialog">
        <form method="post" id="course_form" enctype="multipart/form-data">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">AJOUT</h4>
                </div>
                <div class="modal-body">
                    <label>Nom</label>
                    <input type="text" name="nom_comptes" id="nom_comptes" class="form-control" />
                    <br />
                    <label>Prénom</label>
                    <input type="text" name="prenom_comptes" id="prenom_comptes" class="form-control" />
                    <br /> 
                    <label>Nom Utilisateur</label>
                    <input type="text" name="username_comptes" id="username_comptes" class="form-control" />
                    <br />
                    <label>Mot de passe</label>
                    <input type="password" name="password_comptes" id="password_comptes" class="form-control" />
                    <br /> <label>Adresse</label>
                    <input type="text" name="adresse_comptes" id="adresse_comptes" class="form-control" />
                    <br />
                    <label>N° Téléphone</label>
                    <input type="number" name="telephone_comptes" id="telephone_comptes" class="form-control" />
                    <br /> 
                    <label>N° Session Restos</label>
                    <input type="text" name="session_restos" id="session_restos" class="form-control" />
                    <br /> 
                </div>
                <div class="modal-footer">
                    <input type="hidden" name="id_comptes" id="id_comptes" />
                    <input type="hidden" name="operation" id="operation" />
                    <input type="submit" name="action" id="action" class="btn btn-primary" value="Add" />
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Fermer</button>
                </div>
            </div>
        </form>
    </div>

   
</div>
<!-- SCRIPT JS + AJAX exécutant l'ajout d'un utilisateur en communicant avec les fichiers php -->
<script type="text/javascript" language="javascript" >
$(document).ready(function(){
    $('#add_button').click(function(){
        $('#course_form')[0].reset();
        $('.modal-title').text("Ajouter un utilisateur");
        $('#action').val("Ajouter");
        $('#operation').val("Add");
    });

    
    
    var dataTable = $('#course_table').DataTable({
        "paging":true,
        "processing":true,
        "serverSide":true,
        "order": [],
        "info":true,
        "ajax":{
            url:"fetch.php",
            type:"POST"
               },
        "columnDefs":[
            {
                "targets":[0,3,4],
                "orderable":false,
            },
        ],    
    });

    $(document).on('submit', '#course_form', function(event){
        event.preventDefault();
        var nom_comptes = $('#nom_comptes').val();
        var prenom_comptes = $('#prenom_comptes').val();
        var username_comptes = $('#username_comptes').val();
        var password_comptes = $('#password_comptes').val();
        var adresse_comptes = $('#adresse_comptes').val();
        var telephone_comptes = $('#telephone_comptes').val();
        var session_restos = $('#session_restos').val();
        
        if(nom_comptes != '' && prenom_comptes != '' && username_comptes != '' && password_comptes != '' && adresse_comptes != '' && telephone_comptes != '' && session_restos != '')
        {
            $.ajax({
                url:"insert.php",
                method:'POST',
                data:new FormData(this),
                contentType:false,
                processData:false,
                success:function(data)
                {
                    $('#course_form')[0].reset();
                    $('#userModal').modal('hide');
                    dataTable.ajax.reload();
                }
            });
        }
        else
        {
            alert("Oups, vous avez sans-doute oublié de remplirun champ");
        }
    });
    //Mis à jour des informations utilisateurs
    $(document).on('click', '.update', function(){
        var id_comptes = $(this).attr("id_comptes");
        $.ajax({
            url:"fetch_single.php",
            method:"POST",
            data:{id_comptes:id_comptes},
            dataType:"json",
            success:function(data)
            {
                $('#userModal').modal('show');
                $('#nom_comptes').val(data.nom_comptes);
                $('#prenom_comptes').val(data.prenom_comptes);
                $('#username_comptes').val(data.username_comptes);
                $('#password_comptes').val(data.password_comptes);
                $('#adresse_comptes').val(data.adresse_comptes);
                $('#telephone_comptes').val(data.telephone_comptes);
                $('#session_restos').val(data.session_restos);
                $('#course').val(data.course);
                $('.modal-title').text("Mise à jour des informations de l'utilisateur");
                $('#id_comptes').val(id_comptes);
                $('#action').val("Sauvegarder");
                $('#operation').val("Edit");
            }
        })
    });
    //Suppression d'un utilisateur
    $(document).on('click', '.delete', function(){
        var id_comptes = $(this).attr("id_comptes");
        if(confirm("Êtes-vous sûr de vouloir supprimer cet utilisateur ?"))
        {
            $.ajax({
                url:"delete.php",
                method:"POST",
                data:{id_comptes:id_comptes},
                success:function(data)
                {
                    dataTable.ajax.reload();
                }
            });
        }
        else
        {
            return false;   
        }
    });
    
    
});
</script>


<center>
<style>
.button {
  background-color: #4CAF50;
  border: none;
  color: white;
  padding: 15px 32px;
  text-align: center;
  text-decoration: none;
  display: inline-block;
  font-size: 16px;
  margin: 4px 2px;
  cursor: pointer;
}
</style>


<!-- Bouton permettant de revenir à l'acceuil -->
<a class="button" href="../tools_admin.php">Revenir à l'accueil</a>

</center>