<?php 
class Database {
    
    private static $dbhost = "localhost";
    private static $dbname = "basemcdreunion";
    private static $dbUser = "root";
    private static $dbUserPassword = "";

    private static $connection = null;

    //Fonction qui gère la phase de connexion
    public static function connect(){

        try{

            self::$connection = new PDO("mysql:host=".self::$dbhost.";dbname=".self::$dbname,self::$dbUser, self::$dbUserPassword);

        } catch(PDOException $e){

            die($e->getMessage());

        }
            return self::$connection ;

        }

    //Fonction qui gère la phase de deconnexion
    public static function disconnect() {   

        self::$connection = null ;    
        
    }

    //Fonction qui donne le nombre d'enregistrements retourné par l'instruction sql. Retourne un integer
    function nbLignesBDD($sql) {

        $arr = self::$connection->query($sql)->fetchAll();
        return count($arr);
        
    }

    //Fonction qui lis un seul enregistrement par l'instruction sql. Retourne un array 1D  
    function lireligneBDD($pdo, $sql) {
        
        $arr = self::$connection->query($sql)->fetchAll();		// array 2D
        if (count($arr) == 0) {
            $retour = false;
        } else {
            $retour = $arr[0] ;		// attention, 1er enreg si plusieurs !
        };
        
        return $retour;
        
    }

    //Fonction qui lis plusieurs enregistrements par l'instruction sql. Retourne un array 2D
    function lirelignesBDD($pdo, $sql) {
        
        $arr = self::$connection->query($sql)->fetchAll();
        
        return $arr;
    }

    //Fonction qui execute un SQL de type INSERT, UPDATE, DELETE. Retourne un nombre de lignes impactées. 
    function execBDD($pdo, $sql) {
        
        $rowCount = self::$connection->exec($sql);
        
        return $rowCount;
    } 
    
}
