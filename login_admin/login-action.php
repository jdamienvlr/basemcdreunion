<?php
namespace Phppot;

use \Phppot\Member;
if (! empty($_POST["login"])) {
    session_start();
    $username = filter_var($_POST["username_admin"], FILTER_SANITIZE_STRING);
    $password = filter_var($_POST["password_admin"], FILTER_SANITIZE_STRING);
    require_once (__DIR__ . "./class/Member.php");
    
    $member = new Member();
    $isLoggedIn = $member->processLogin($username, $password);
    if (! $isLoggedIn) {
        $_SESSION["errorMessage"] = "Invalide";
        header('Location: index_login.php');
    }else{
        header('Location: tools_admin.php');}
    exit();
}