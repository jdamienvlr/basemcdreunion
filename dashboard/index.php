

<!DOCTYPE html>
<html lang="fr">


  <head>

  <!--===============================================================================================-->

        <!--===============================================================================================-->	
        <!--===============================================================================================-->	
        <!--=======================================DATA TABLE BOOTSTRAP 5====================================-->	
     
              <!--Author : JD-->
              <link rel="stylesheet" type="text/css"  href="DataTable/css/buttons.dataTables.min.css" />
              <link rel="stylesheet" type="text/css"  href="DataTable/css/jquery.dataTables.min.css" />

              <script src="DataTable/js/jquery-3.5.1.js"></script>
              <script src="DataTable/js/jquery.dataTables.min.js"></script>
              <script src="DataTable/js/dataTables.buttons.min.js"></script>
              <script src="DataTable/js/jszip.min.js"></script>
              <script src="DataTable/js/pdfmake.min.js"></script>
              <script src="DataTable/js/vfs_fonts.js"></script>
              <script src="DataTable/js/buttons.html5.min.js"></script>
              <script src="DataTable/js/buttons.print.min.js"></script>
        <!--===============================================================================================-->	
        <!--===============================================================================================-->	
  
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="css/bootstrap.min.css" />
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.4.1/font/bootstrap-icons.css"/>
    <link rel="stylesheet" href="css/dataTables.bootstrap5.min.css" />
    <link rel="stylesheet" href="css/style.css" />
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <title>Dashboard McDonald's : Suivi CA Restos</title>
  </head>
  <body onload='default_DataBase()'>

      
    <!-- top navigation bar -->
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
      <div class="container-fluid">

     
        <button
          class="navbar-toggler"
          type="button"
          data-bs-toggle="offcanvas"
          data-bs-target="#sidebar"
          aria-controls="offcanvasExample"
        >

          <span class="navbar-toggler-icon" data-bs-target="#sidebar"></span>

        
        
          <?php

session_start();
if(!empty($_SESSION["userId"])) {
    require_once '../login/view/dashboard_displayname.php';
    
} else {
  echo "bruh";
	
}

?>




        </button>
      
      <!--
       
        <button
          class="navbar-toggler"
          type="button"
          data-bs-toggle="collapse"
          data-bs-target="#topNavBar"
          aria-controls="topNavBar"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
       <span class="navbar-toggler-icon"></span>
        </button>-->
        <div class="collapse navbar-collapse" id="topNavBar">
      
      
          <form class="d-flex ms-auto my-3 my-lg-0">
            <div class="input-group">
            <!--  <input
                class="form-control"
                type="search"
                placeholder="Search"
                aria-label="Search"
              />
              <button class="btn btn-primary" type="submit">
                <i class="bi bi-search"></i>
              </button>-->
             <!--
            </div>
          </form>

        
          <ul class="navbar-nav">
            <li class="nav-item dropdown">
              <a
                class="nav-link dropdown-toggle ms-2"
                href="#"
                role="button"
                data-bs-toggle="dropdown"
                aria-expanded="false"
              >
                <i class="bi bi-person-fill"></i>
              </a> 
              <ul class="dropdown-menu dropdown-menu-end">
              
                <li><a class="dropdown-item" href="#"> </a></li>
                <li><a class="dropdown-item" href="#">Action n°1</a></li>
                <li>
                  <a class="dropdown-item" href="#">Action n°2</a>
                </li>
              </ul>
            </li>
          </ul>
         
        </div>
-->
</form>
      </div>
    </nav>
   
 <!-- top navigation bar -->
    <!-- offcanvas -->
    <div
      class="offcanvas offcanvas-start sidebar-nav bg-dark"
      tabindex="-1"
      id="sidebar"
    > 



    
      <div class="offcanvas-body p-0">
      
        <nav class="navbar-dark">


<!-- Nav bar-->
        <ul class="navbar-nav">
    
           <li class="nav-item dropdown">
             <br/><br/>
          <a
                class="nav-link dropdown-toggle ms-2"
                href="#"
                role="button"
                data-bs-toggle="dropdown"
                aria-expanded="false"
              ><b><Font size="4pt">  Choix des outils : 
                <i class="bi bi-person-fill"></i>
              </a></FONT></b>
              <ul class="dropdown-menu dropdown-menu-end">
              
                <li> <a class="dropdown-item" href="FEUILLE-DE-CAISSE" onclick="document.location='indexFDC.php' ; return false" >Feuille de caisses</a>
                  <!--<a class="dropdown-item" href="indexFDC.php">Feuille de caisse</a></li>-->
                <li>
                  <a class="dropdown-item" href="indexPMX.php">PMIX Au Mois</a>
                </li>
                <a class="dropdown-item" href="indexPetitDej.php">Petit Dej Au Mois</a>
              </ul>
            </li>
          </ul>






          <ul class="navbar-nav">
            <li><br/><br/>
              <div class="text-muted small fw-bold text-uppercase px-3">
                CORE
              </div>
            </li>






            
            <li>
              <a href="#" class="nav-link px-3 active">
                <span class="me-2"><i class="bi bi-speedometer2"></i></span>
                <span>Dashboard</span>
              </a>
            </li>



                 
            

            <li class="my-4"><hr class="dropdown-divider bg-light" /></li>
            <li>
              <div class="text-muted small fw-bold text-uppercase px-3 mb-3">
                Interface
              </div>
            </li>
            <li>
              <a
                class="nav-link px-3 sidebar-link"
                data-bs-toggle="collapse"
                href="#layouts"
               >
                <!-- <span class="me-2"><i class="bi bi-layout-split"></i></span>
                 <span>Layouts</span>
                <span class="ms-auto">
                  <span class="right-icon">
                    <i class="bi bi-chevron-down"></i>
                  </span>
                </span> --> 
              </a>
              <div class="collapse" id="layouts">
                <ul class="navbar-nav ps-3">
                  <li>
                    <a href="#" class="nav-link px-3">
                      <span class="me-2"
                        ><i class="bi bi-speedometer2"></i
                      ></span>
                      <span>Dashboard</span>
                    </a> 
                  </li>
                </ul>
              </div>
            </li>
            <li>
              <!-- <a href="#" class="nav-link px-3">
                <span class="me-2"><i class="bi bi-book-fill"></i></span>
                <span>Pages</span>
              </a> -->
            </li>
            <li class="my-4"><hr class="dropdown-divider bg-light" /></li>
            <li>
              <div class="text-muted small fw-bold text-uppercase px-3 mb-3">
                Addons
              </div>
            </li>
            <!-- <li>
              <a href="#" class="nav-link px-3">
                <span class="me-2"><i class="bi bi-graph-up"></i></span>
                <span>Charts</span>
              </a>
            </li>
            <li>
              <a href="#" class="nav-link px-3">
                <span class="me-2"><i class="bi bi-table"></i></span>
                <span>Tables</span>



              
              </a>
            </li> -->
          </ul>
        </nav>
      </div>
    </div>
    <!-- offcanvas -->
    <main class="mt-5 pt-3">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-12">
            <br/>
            <h4>Dashboard</h4>
           
          </div>
        </div>
        <div class="row">
          <div class="col-md-3 mb-3">
            <div class="card bg-primary text-white h-100">
              <div class="card-body py-5">Tâches</div>
              <div class="card-footer d-flex">
                Voir Détails
                <span class="ms-auto">
                  <i class="bi bi-chevron-right"></i>
                </span>
              </div>
            </div>
          </div>
          <div class="col-md-3 mb-3">
            <div class="card bg-warning text-dark h-100">
              <div class="card-body py-5">Tâches en attentes</div>
              <div class="card-footer d-flex">
              Voir Détails
                <span class="ms-auto">
                  <i class="bi bi-chevron-right"></i>
                </span>
              </div>
            </div>
          </div>
          <div class="col-md-3 mb-3">
            <div class="card bg-success text-white h-100">
              <div class="card-body py-5">Tâches finis</div>
              <div class="card-footer d-flex">
              Voir Détails
                <span class="ms-auto">
                  <i class="bi bi-chevron-right"></i>
                </span>
              </div>
            </div>
          </div>
          <div class="col-md-3 mb-3">
            <div class="card bg-danger text-white h-100">
              <div class="card-body py-5">Tâches en retard</div>
              <div class="card-footer d-flex">
                Voir Details
                <span class="ms-auto">
                  <i class="bi bi-chevron-right"></i>
                </span>
              </div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-6 mb-3">
            <div class="card h-100">
              <div class="card-header">
                <span class="me-2"><i class="bi bi-bar-chart-fill"></i></span>
              Temps utilisation dashboard
              </div>
              <div class="card-body">
                <canvas class="chart" width="400" height="200"></canvas>
              </div>
            </div>
          </div>
          <div class="col-md-6 mb-3">
            <div class="card h-100">
              <div class="card-header">
                <span class="me-2"><i class="bi bi-bar-chart-fill"></i></span>
                Exports Excels
              </div>
              <div class="card-body">
                <canvas class="chart" width="400" height="200"></canvas>
              </div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12 mb-3">
            <div class="card">
              <div class="card-header">Suivi ca
                
            
<center>



  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
 <!-- <link rel="stylesheet" href="/resources/demos/style.css">-->
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  
 
<p>Sélectionner une Date : </p>
<br/>
<p>Format : (Année-mois-Jour) </p>



<!-- <input type="text"  value="20210201" id="displayDate" /> -->
<input type="date" id='dateTake'/>

&nbsp;
<input type="date" id='dateTake2' onchange='initialise_datatable()'/>

<input type="hidden" id="submittedDate" />

<!-- <button value="id_compte" onchange='id_comptes()'> TEST </button> -->

 






</center>
              
              </div>
<br/><br/>
            
                
              <center>
              <div class="card-body">
                <div class="table-responsive">
                  <div class="limiter"></div>

             


                   <!---<div class="container-table100">--> 
                   
                      <div class="wrap-table100">
<br/><br/>
<div id="test">
<table id='empTable' class='display dataTable'>
                <thead>
                <tr>
                    <th>Code Restos</th>
                    <th>Nom Restos</th>
                    <th>Ventes Réalisées</th>
                    <!--<th>Ventes N-1</th>-->
				          	<th>TAC Réalisées</th>
                   <!-- <th>TAC N-1</th>-->
                </tr>
                </thead>
               
                
            </table>
        </div>

 

<script>


function initialise_datatable() {
  var date = document.getElementById('dateTake');
  var annee = date.value.substr(0,4);
  var mois = date.value.substr(5,2);
  var jour = date.value.substr(8,2);
  var dateF = annee+mois+jour;

  var date2 = document.getElementById('dateTake2');
  var annee2 = date2.value.substr(0,4);
  var mois2 = date2.value.substr(5,2);
  var jour2 = date2.value.substr(8,2);
  var dateF2 = annee2+mois2+jour2;

  if( !dateF ){

alert(" Tu as oublié de remplir la première date. C'est pas malin 🤓.  ")
return false;
}


 if( $.fn.dataTable.isDataTable( '#empTable' )){
   table = $('#empTable').DataTable();
  table.destroy();
  $('#empTable').DataTable( {
      
      'processing': true,
      'retrieve': true,
       'serverSide': true,
        'serverMethod': 'post',
         'ajax': {
          'url':'configAjax/suivica.php',
          'data':{
            'dateF': dateF,
            'dateF2': dateF2,
          }
                },
                dom: 'lBfrtip',
        buttons: [
  
            'copy', 'csv', 'excel', 'pdf', 'print'
        ],
        'columns': [
                  { data: 'stpsto' },
                  { data: 'stpnam' },
                  { data: 'CA' },
                  { data: 'TAC' },
                  
                ]
            })
 }else{
  $('#empTable').DataTable( {
      
      'processing': true,
      'retrieve': true,
       'serverSide': true,
        'serverMethod': 'post',
         'ajax': {
          'url':'configAjax/suivica.php',
          'data':{
            'dateF': dateF,
            'dateF2': dateF2,
          }
                },
                dom: 'lBfrtip',
        buttons: [
  
            'copy', 'csv', 'excel', 'pdf', 'print'
        ],
        'columns': [
                  { data: 'stpsto' },
                  { data: 'stpnam' },
                  { data: 'CA' },
                  { data: 'TAC' },
                  
                ]
            })
          }
 
}


  
function default_DataBase() {
  

 
  $('#empTable').DataTable( {
      
    'processing': true,
      'retrieve': true,
       'serverSide': true,
        'serverMethod': 'post',
         'ajax': {
          'url':'configAjax/idComptes.php',
          
                },
        dom: 'lBfrtip',
        buttons: [
  
            'copy', 'csv', 'excel', 'pdf', 'print'
        ],
        'columns': [
                  { data: 'stpsto' },
                  { data: 'stpnam' },
                  { data: 'CA' },
                  { data: 'TAC' },
                  
                ]
            })
          }
 


</script>




                      
                      </div>
                    </div>
                 <!-- </div>-->
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </center>
    </main>
    <script src="./js/bootstrap.bundle.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/chart.js@3.0.2/dist/chart.min.js"></script>
    
    <script src="./js/dataTables.bootstrap5.min.js"></script>
    <script src="./js/script.js"></script>
  </body>


  
</html>
