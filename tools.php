<!DOCTYPE html>





<html lang="en">

<head>

	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">   
   
    <!-- Mobile Metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
	
 
     <!-- Site Metas -->
    <title>McDonald's Réunion</title>  
    <meta name="keywords" content="">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Site Icons -->
    
    <link rel="icon" href="images/logo.png">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">    
	<!-- Site CSS -->
    <link rel="stylesheet" href="css/style.css">    
    <!-- Responsive CSS -->
    <link rel="stylesheet" href="css/responsive.css">
    <!-- Custom CSS -->
    <link rel="stylesheet" href="css/custom.css">

    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<!-- Affiche les numéros des restaurants en fonction de l'id de l'utilisateur -->
<!-- Debut du corps du site -->
<body>
	<!-- Start header -->
	<header class="top-navbar">
		<nav class="navbar navbar-expand-lg navbar-light bg-light">
		<?php

session_start();
if(!empty($_SESSION["userId"])) {
    require_once 'login/view/dashboard.php';
} else {
	require_once 'login/view/login-form.php';
	
}

?>

			<div class="container">

		
				<a class="navbar-brand" href="Outils">
					<img  src="http://logok.org/wp-content/uploads/2014/06/McDonalds-logo.png" alt="Mcdlogo" width="150px" height="120px">
					McDonald's Réunion   
				</a>
				<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbars-rs-food" aria-controls="navbars-rs-food" aria-expanded="false" aria-label="Toggle navigation">
				  <span class="navbar-toggler-icon"></span>
				</button>
				<div class="collapse navbar-collapse" id="navbars-rs-food">
															<!-- Liste déroulantes menant vers différentes pages HTML (11 pages -> 11 restaurants) -->

					<ul class="navbar-nav ml-auto">
						<li class="nav-item active"><a class="nav-link" href="Outils">Outils</a></li>
						
						<li class="nav-item dropdown">
						<!-- <a class="nav-link dropdown-toggle" href="#" id="dropdown-a" data-toggle="dropdown">Restaurants</a>
							<div class="dropdown-menu" aria-labelledby="dropdown-a">
								<a class="dropdown-item" href="">McDonald’s Ste-Marie</a>
								<a class="dropdown-item" href="">McDonald’s Le Port</a>
								<a class="dropdown-item" href="">McDonald’s St Denis</a>
								<a class="dropdown-item" href="">McDonald’s St Benoît</a>
								<a class="dropdown-item" href="">McDonald’s St Pierre</a>
								<a class="dropdown-item" href="">McDonald’s St Pierre-400</a>
								<a class="dropdown-item" href="">McDonald’s La Possession</a>
								<a class="dropdown-item" href="">McDonald’s St Louis</a>
								<a class="dropdown-item" href="">McDonald’s Éperon</a>
								<a class="dropdown-item" href="">McDonald’s Carrefour</a>
								<a class="dropdown-item" href="">McDonald’s St Joseph</a>
							</div>
						</li> -->
																		<!-- Accéder au page A propos et authentification -->

						<!-- <li class="nav-item"><a class="nav-link" href="Contact">Contact</a></li>
						<li class="nav-item"><a class="nav-link" href="A-Propos">A Propos</a></li> -->
						<li class="nav-item"><a class="nav-link" href="login/logout.php">Se Déconnecter</a></li>
					</ul>
				</div>
			</div>
		</nav>
	</header>
	<!-- FIN du header -->
	
	
	
	<!-- Debut des slides , 3 au total-->
	<div id="slides" class="cover-slides">
		<ul class="slides-container">
			<li class="text-center">
					<!-- Arrière plan du slide n°1 -->
				<img src="images/slider-01.jpg" alt="">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<!--  Titre Slide -->
							<br><br><br><br><h1 class="m-b-20"><strong>Listes des Outils <br> Disponible : <br></strong>
							</h1><br>
							<!-- Corps du Slide -->
							<p><a class="btn btn-lg btn-circle btn-outline-new-white" href="dashboard_MCD/suiviCA.php"> &nbsp;&nbsp;Accéder aux Outils&nbsp;&nbsp;</a></p>
						</div>
					</div>
				</div>
			</li>
			<li class="text-center">
				<!-- Arrière plan du slide n°2 -->
				<img src="images/slider-02.jpg" alt="">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<!--  Titre Slide -->
							<br><br><br><br><br><h1 class="m-b-20"><strong>Listes des Outils <br> Disponible : <br></strong>
							</h1><br><!-- Corps du Slide -->	
							<p><a class="btn btn-lg btn-circle btn-outline-new-white" href="dashboard_MCD/suiviCA.php"> &nbsp;&nbsp;Accéder aux Outils&nbsp;&nbsp;</a></p>
						</div>
					</div>
				</div>
			</li>
			<li class="text-center">
				<!-- Arrière plan du slide n°3 -->
				<img src="images/slider-03.jpg" alt="">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<!--  Titre Slide -->
							<br><br><br><br><br><h1 class="m-b-20"><strong>Listes des Outils <br> Disponible : <br></strong>
							</h1><br><!-- Corps du Slide -->	
							<p><a class="btn btn-lg btn-circle btn-outline-new-white" href="dashboard_MCD/suiviCA.php"> &nbsp;&nbsp;Accéder aux Outils&nbsp;&nbsp;</a></p>
						</div>
					</div>
				</div>
			</li>
		</ul>
		<div class="slides-navigation">
			<a href="#" class="next"><i class="fa fa-angle-right" aria-hidden="true"></i></a>
			<a href="#" class="prev"><i class="fa fa-angle-left" aria-hidden="true"></i></a>
		</div>
	</div>
<!-- FIN slides -->
	
	<!-- A Porpos -->
	<div class="about-section-box">
		<div class="container">
			<div class="row">
				<div class="col-lg-6 col-md-6 col-sm-12">
					<!-- Image au centre du page web -->
					<img src="images/about-img.jpg" alt="" class="img-fluid">
				</div>
				<div class="col-lg-6 col-md-6 col-sm-12 text-center">
						<!-- partie correspondant aux texte situé au centre du site web -->

					<div class="inner-column">
						<h1><span>McDonald’s </span> dans le monde!</h1>
						<h4>Little Story</h4>
						<p>McDonald’s est la plus grande chaîne de restauration rapide dans le monde, servant près de 69 millions de clients chaque jour. </p>
						<p>L’histoire de McDonald’s commence en 1955, avec l’ouverture de la première franchise McDonald’s à Des Plaines dans l’Illinois aux Etats-Unis. Dix ans plus tard, plus de 700 restaurants McDonald’s ouvriront à travers le pays. Aujourd’hui, McDonald’s compte plus de 36 000 restaurants dans le monde.</p>
						<a class="btn btn-lg btn-circle btn-outline-new-white" href="dashboard_MCD/suiviCA.php">Outils</a>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- FIN About -->
	
	<!-- DEBUT QT -->
	<div class="qt-box qt-background">
		<div class="container">
			<div class="row">
				<div class="col-md-8 ml-auto mr-auto text-left">
					<center><p class="lead ">
																		<!-- Textes situé juste avant le bas de page du site web -->

						" Nos Equipes à vos Côtés! "
					</p></center>
				</div>
			</div>
		</div>
	</div>
	<!-- FIN QT -->
	<!-- FIN Contact info -->
	
	
	<!-- Debut Contact info -->
	<div class="contact-imfo-box">
		<div class="container">
			<div class="row">
				<div class="col-md-4">
					<i class="fa fa-volume-control-phone"></i>
					<div class="overflow-hidden">
						<h4>Telephone</h4>
						<p class="lead">
							+262.692.343.030 
						</p>
					</div>
				</div>
				<div class="col-md-4">
					<i class="fa fa-envelope"></i>
					<div class="overflow-hidden">
						<h4>Email</h4>
						<p class="lead">
							rgalap@mcd-reunion.com
						</p>
					</div>
				</div>
				<div class="col-md-4">
					<i class="fa fa-map-marker"></i>
					<div class="overflow-hidden">
						<h4>Adresse</h4>
						<p class="lead">
							1 rue Lobellie 97419 La Possession
						</p>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- FIN Contact info -->
	
	<!-- Debut Footer (bas de page) -->
	<footer class="footer-area bg-f">
		<div class="container">
			<div class="row">
				<div class="col-lg-3 col-md-6">
					<h3>A Propos</h3>
					<p>Ce site web à pour but de permettre aux utilisateurs de consulter les outils disponibles en fonction de leurs besoins (Produits Mix, Suivi des Chiffres d'Affaires, Feuille de caisse, Petit déjeuner).</p>
				</div>
				<div class="col-lg-3 col-md-6">
					<h3>Horaires</h3>
					<p><span class="text-color">Lundi-Vendredi: </span>7H30-18H</p>
					<p><span class="text-color">Samedi-Dimanche :</span> Fermer</p>
				</div>
				<div class="col-lg-3 col-md-6">
					<h3>Contact</h3>
					<p class="lead">1 rue Lobellie 97419 La Possession</p>
					<p class="lead"><a href="#">+262.692.343.030</a></p>
					<p><a href="#"> rgalap@mcd-reunion.com</a></p>
				</div>
				<div class="col-lg-3 col-md-6">
					<!-- <h3>Envoyez un Mail</h3>
					<div class="subscribe_form">
						<form class="subscribe_form">
							<input name="EMAIL" id="subs-email" class="form_input" placeholder="Adresses Mail..." type="email">
							<button type="submit" class="submit">Envoyez</button>
							<div class="clearfix"></div>
						</form> -->
					</div>
					<!-- Icones réseaux sociaux -->
					<ul class="list-inline f-social">
						<!-- <li class="list-inline-item"><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
						<li class="list-inline-item"><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
						<li class="list-inline-item"><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
						<li class="list-inline-item"><a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
						<li class="list-inline-item"><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li> -->
					</ul>
				</div>
			</div>
		</div>
		
		<div class="copyright">
			<div class="container">
				<div class="row">
					<div class="col-lg-12">
						<p class="company-name">Tout Droit Réservés. &copy; 2021 <a href="#">MacDonald's La Réunion</a> 
					</div>
				</div>
			</div>
		</div>
		
	</footer>
	<!-- FIN Footer (bas de page) -->
	
	<a href="#" id="back-to-top" title="Back to top" style="display: none;">&uarr;</a>

	<!-- Listes fichiers JavaScripts  -->
	<script src="js/jquery-3.2.1.min.js"></script>
	<script src="js/popper.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
    <!-- Liste des PLUGINS -->
	<script src="js/jquery.superslides.min.js"></script>
	<script src="js/images-loded.min.js"></script>
	<script src="js/isotope.min.js"></script>
	<script src="js/baguetteBox.min.js"></script>
	<script src="js/form-validator.min.js"></script>
    <script src="js/contact-form-script.js"></script>
    <script src="js/custom.js"></script>
	<!-- Fin du corps de la page web -->
</body>
</html>