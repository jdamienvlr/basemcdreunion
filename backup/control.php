
<!DOCTYPE html>
<html lang="en"><!-- Basic -->
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">   
   
    <!-- Mobile Metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
 
     <!-- Site Metas -->
    <title>Welcome To McDonald's</title>  
    <meta name="keywords" content="">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Site Icons -->
    <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
    <link rel="apple-touch-icon" href="images/apple-touch-icon.png">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">  
	<link rel="stylesheet" href="css/toolbarSearch.css">    
	<!-- Site CSS -->
    <link rel="stylesheet" href="css/style.css">    
    <!-- Responsive CSS -->
    <link rel="stylesheet" href="css/responsive.css">
    <!-- Custom CSS -->
    <link rel="stylesheet" href="css/custom.css">

<title>DataTables</title>
        <!-- Datatable CSS -->
        <link href='DataTables/datatables.min.css' rel='stylesheet' type='text/css'>

        <!-- jQuery Library -->
        <script src="jquery-3.3.1.min.js"></script>
        
        <!-- Datatable JS -->
        <script src="DataTables/datatables.min.js"></script>
    

    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>
	<!-- Start header -->
	<header class="top-navbar">
		<nav class="navbar navbar-expand-lg navbar-light bg-light">
			<div class="container">
				<a class="navbar-brand" href="Home">
					<img  src="images/McDonalds-logo.png" alt="Mcdlogo" width="150px" height="120px">
					McDonald's   
				</a>
				<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbars-rs-food" aria-controls="navbars-rs-food" aria-expanded="false" aria-label="Toggle navigation">
				  <span class="navbar-toggler-icon"></span>
				</button>
				<div class="collapse navbar-collapse" id="navbars-rs-food">
					<ul class="navbar-nav ml-auto">
						<li class="nav-item active"><a class="nav-link" href="Home">Accueil</a></li>
						<li class="nav-item"><a class="nav-link" href="A-Propos">A Propos</a></li>
						<li class="nav-item dropdown">
							<a class="nav-link dropdown-toggle" href="#" id="dropdown-a" data-toggle="dropdown">Restaurants</a>
							<div class="dropdown-menu" aria-labelledby="dropdown-a">
								<a class="dropdown-item" href="">SQL</a>
								<a class="dropdown-item" href="">GDB</a>
								<a class="dropdown-item" href="">CSV</a>
							</div>
						</li>
						<li class="nav-item"><a class="nav-link" href="Contact">Contact</a></li>
					</ul>
				</div>
			</div>
		</nav>
	</header>
	<!-- End header -->
	
	<!-- Start slides -->
	<div id="slides" class="cover-slides">
		<ul class="slides-container">
			<li class="text-center">
				<img src="images/slider-01.jpg" alt="">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<br><br><br><br><h1 class="m-b-20"><strong>Bienvenue chez <br> Mcdonald's <br></strong>
							</h1>
							<p class="m-b-40">Le Saviez-Vous?  <br> 
							Chaque visite chez McDonald's EST UNIQUE</p>
							<p><a class="btn btn-lg btn-circle btn-outline-new-white" href="#">SUIVIS RESTOS</a></p>
						</div>
					</div>
				</div>
			</li>
			<li class="text-center">
				<img src="images/slider-02.png" alt="">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<br><br><br><br><br><br><h1 class="m-b-20"><strong>Bienvenue chez <br> Mcdonald's <br></strong>
								<p class="m-b-40">La Famille avant tout! <br>
							S'AMUSER avec Mcdonald's & Offre HappyMeal.</p>	
							<p><a class="btn btn-lg btn-circle btn-outline-new-white" href="#">SUIVIS RESTOS</a></p>

						</div>
					</div>
				</div>
			</li>
			<li class="text-center">
				<img src="images/slider-03.jpg" alt="">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<br><br><br><br><br><br><h1 class="m-b-20"><strong>Bienvenue chez <br> Mcdonald's <br></strong>
								<p class="m-b-40">Le DESIGN <br> 
							SATISFAIRE ses fidèles clients.</p>	
							<p><a class="btn btn-lg btn-circle btn-outline-new-white" href="#">SUIVIS RESTOS</a></p>
						</div>
					</div>
				</div>
			</li>
		</ul>
		<div class="slides-navigation">
			<a href="#" class="next"><i class="fa fa-angle-right" aria-hidden="true"></i></a>
			<a href="#" class="prev"><i class="fa fa-angle-left" aria-hidden="true"></i></a>
		</div>
	</div>
	<!-- End slides -->
    <br><br><br><br>
    <center>

<body>


<style>
* {
  box-sizing: border-box;
}

#myInput {
  background-image: url('/css/searchicon.png');
  background-position: 10px 12px;
  background-repeat: no-repeat;
  width: 100%;
  font-size: 16px;
  padding: 12px 20px 12px 40px;
  border: 1px solid #ddd;
  margin-bottom: 12px;
}

#myUL {
  list-style-type: none;
  padding: 0;
  margin: 0;
}

#myUL li a {
  border: 1px solid #ddd;
  margin-top: -1px; /* Prevent double borders */
  background-color: #f6f6f6;
  padding: 12px;
  text-decoration: none;
  font-size: 18px;
  color: black;
  display: block
}

#myUL li a:hover:not(.header) {
  background-color: #eee;
}
</style>
</head>
<body>





<br><br><br><br>

<h2><u><b>Suivi-Produits :</h2></u></b>

<br><br>
<style>
#customers {
  font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
  border-collapse: collapse;
  width: 100%;
}

#customers td, #customers th {
  border: 1px solid #ddd;
  padding: 8px;
}

#customers tr:nth-child(even){background-color: #f2f2f2;}

#customers tr:hover {background-color: #ddd;}

#customers th {
  padding-top: 12px;
  padding-bottom: 12px;
  text-align: left;
  background-color: #ffcc00;
  color: white;
}
</style>
</head>
<center>
  <select class = "nav-link" >
    <option value="0">Select Restos:</option>
    <option value="1">1960</option>
    <option value="2">1961</option>
    <option value="3">1962</option>
    <option value="4">1963</option>
    <option value="5">1964</option>
    <option value="6">1965</option>
    <option value="7">1967</option>
    <option value="8">1968</option>
    <option value="9">1970</option>
  </select>
  <center>

<br>
    
        <div >
            <!-- Table -->
            <table id='empTable' class='display dataTable'>
                <thead>
                <tr>
                    <th>CODWRIN</th>
                    <th>LIB</th>
                    <th>CODMRIN</th>
                </tr>
                </thead>
                
            </table>
        </div>
        
        <!-- Script -->
        <script>
        $(document).ready(function(){
            $('#empTable').DataTable({
                'processing': true,
                'serverSide': true,
                'serverMethod': 'post',
                'ajax': {
                    'url':'ajaxfile.php'
                },
                'columns': [
                    { data: 'PRB_CODWRIN' },
                    { data: 'PRB_LIB' },
                    { data: 'PRG_CODMRIN' },
                ]
            });
        });
        </script>

<br>




<br><br><br><br>



<center>
<form action="mail.php" method="post" enctype="multipart/mixed">
<div class="col-lg-3 col-md-6">
					<h3>Vérification Restos:</h3>
					<div class="subscribe_form">
						<form class="subscribe_form">

	<label for="nom">Votre nom</label>
	<input type="text" name="nom" class="form_input" placeholder="Votre Nom..." required/><br>
	<label for="email" >Votre E-mail</label><br>
	<input type="email" name="email" required class="form_input" placeholder="Adresses Mail..." /><br>
	<label for="message">Votre demande</label><br>
	<textarea name="message" rows="2" cols="50" class="form_input" placeholder="Adresses Mail..." required ></textarea><br>
	<label for='fichier'>Ajouter une pièce jointe <span>(jpeg ou pdf, max 2Mo)</span></label><br>
        <input type="file" name="fichier" id="fichier"><br><br>
	<button type="submit" value="Envoyer">ENVOYEZ</button>
</form>

<br><br><br>

</center>

	
						</form>
                    </div>
                    
               
    <br><br><br><br><br><br>
    









	<!-- Start Contact info -->
	<div class="contact-imfo-box">
		<div class="container">
			<div class="row">
				<div class="col-md-4">
					<i class="fa fa-volume-control-phone"></i>
					<div class="overflow-hidden">
						<h4>Telephone</h4>
						<p class="lead">
							0692XXXXXX
						</p>
					</div>
				</div>
				<div class="col-md-4">
					<i class="fa fa-envelope"></i>
					<div class="overflow-hidden">
						<h4>Email</h4>
						<p class="lead">
							island@mcd-reunion.com
						</p>
					</div>
				</div>
				<div class="col-md-4">
					<i class="fa fa-map-marker"></i>
					<div class="overflow-hidden">
						<h4>Adresse</h4>
						<p class="lead">
							Zac 2000, Le Port.
						</p>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- End Contact info -->
	
	<!-- Start Footer -->
	<footer class="footer-area bg-f">
		<div class="container">
			<div class="row">
				<div class="col-lg-3 col-md-6">
					<h3>A Propos</h3>
					<p>XXX.</p>
				</div>
				<div class="col-lg-3 col-md-6">
					<h3>Horaires</h3>
					<p><span class="text-color">Lundi: </span>8h-17h</p>
					<p><span class="text-color">Mardi-Mercredi :</span> 8h-18h</p>
					<p><span class="text-color">Jeudi-Vendredi :</span> 8h-17h30</p>
					<p><span class="text-color">Samedi-Dimanche :</span> Fermer</p>
				</div>
				<div class="col-lg-3 col-md-6">
					<h3>Contact</h3>
					<p class="lead">Le Port, Zac 2000</p>
					<p class="lead"><a href="#">0692XXXXXX</a></p>
					<p><a href="#"> island@mcd-reunion.com</a></p>
				</div>
				<div class="col-lg-3 col-md-6">
					<h3>Envoyez un Mail</h3>
					<div class="subscribe_form">
						<form class="subscribe_form">
							<input name="EMAIL" id="subs-email" class="form_input" placeholder="Adresses Mail..." type="email">
							<button type="submit" class="submit">Envoyez</button>
							<div class="clearfix"></div>
						</form>
					</div>
					<ul class="list-inline f-social">
						<li class="list-inline-item"><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
						<li class="list-inline-item"><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
						<li class="list-inline-item"><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
						<li class="list-inline-item"><a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
						<li class="list-inline-item"><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
					</ul>
				</div>
			</div>
		</div>
		
		<div class="copyright">
			<div class="container">
				<div class="row">
					<div class="col-lg-12">
						<p class="company-name">Tout Droit Réservés. &copy; 2020 <a href="#">MacDonald's</a> 
					</div>
				</div>
			</div>
		</div>
		
	</footer>
	<!-- End Footer -->
	
	<a href="#" id="back-to-top" title="Back to top" style="display: none;">&uarr;</a>

	<!-- ALL JS FILES -->
	
	<script src="js/popper.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
    <!-- ALL PLUGINS -->
	<script src="js/jquery.superslides.min.js"></script>
	<script src="js/images-loded.min.js"></script>
	<script src="js/isotope.min.js"></script>
	<script src="js/baguetteBox.min.js"></script>
	<script src="js/form-validator.min.js"></script>
    <script src="js/contact-form-script.js"></script>
	<script src="js/custom.js"></script>
</body>
</html>