<?php
include 'config.php';

## Read value
$draw = $_POST['draw'];
$row = $_POST['start'];
$rowperpage = $_POST['length']; // Rows display per page
$columnIndex = $_POST['order'][0]['column']; // Column index
$columnName = $_POST['columns'][$columnIndex]['data']; // Column name
$columnSortOrder = $_POST['order'][0]['dir']; // asc or desc
$searchValue = $_POST['search']['value']; // Search value

## Search 
$searchQuery = " ";
if($searchValue != ''){
	$searchQuery = " and (hsaday like '%".$searchValue."%' or 
    hsatim like '%".$searchValue."%' or 
    hsasal like'%".$searchValue."%' or
    hsatac like'%".$searchValue."%' or
    hsadns like'%".$searchValue."%' or
    hsadtc like'%".$searchValue."%' or
    hsachw like'%".$searchValue."%' or
    hsacgp like'%".$searchValue."%' or
    hsafir like'%".$searchValue."%' or
    hsadfu like'%".$searchValue."%') ";
    
}

## Total number of records without filtering
$sel = mysqli_query($con,"select count(*) as allcount from cshhsap0_1960");
$records = mysqli_fetch_assoc($sel);
$totalRecords = $records['allcount'];

## Total number of records with filtering
$sel = mysqli_query($con,"select count(*) as allcount from cshhsap0_1960 WHERE 1 ".$searchQuery);
$records = mysqli_fetch_assoc($sel);
$totalRecordwithFilter = $records['allcount'];

## Fetch records
$empQuery = "select * from cshhsap0_1960 WHERE 1 ".$searchQuery." order by ".$columnName." ".$columnSortOrder." limit ".$row.",".$rowperpage;
$empRecords = mysqli_query($con, $empQuery);
$data = array();

while ($row = mysqli_fetch_assoc($empRecords)) {
    $data[] = array(
    		"hsaday"=>$row['hsaday'],
    		"hsatim"=>$row['hsatim'],
    		"hsasal"=>$row['hsasal'],
            "hsatac"=>$row['hsatac'],
    		"hsadns"=>$row['hsadns'],
    		"hsadtc"=>$row['hsadtc'],
            "hsachw"=>$row['hsachw'],
    		"hsacgp"=>$row['hsacgp'],
    		"hsafir"=>$row['hsafir'],
            "hsadfu"=>$row['hsadfu'],

    	);
}

## Response
$response = array(
    "draw" => intval($draw),
    "iTotalRecords" => $totalRecords,
    "iTotalDisplayRecords" => $totalRecordwithFilter,
    "aaData" => $data
);

echo json_encode($response);
