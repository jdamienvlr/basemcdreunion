
  <!DOCTYPE html>
<html lang="en">
    <!--Author : JDamien VLR-->

<!-- Début Head -->
<head>
    <!-- Meta tags Requis-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="au theme template">
    <meta name="author" content="Hau Nguyen">
    <meta name="keywords" content="au theme template">

    <!-- <link rel="icon" href="../../mcdonalds/images/logo.png"> -->
    <!-- Titre de la Page-->
    <title>Dashboard </title>

    <link rel="icon" href="images/logo.png">

    <!-- FrondEnd CSS - Style-->
    <link href="css/font-face.css" rel="stylesheet" media="all">
    <link href="vendor/font-awesome-4.7/css/font-awesome.min.css" rel="stylesheet" media="all">
    <link href="vendor/font-awesome-5/css/fontawesome-all.min.css" rel="stylesheet" media="all">
    <link href="vendor/mdi-font/css/material-design-iconic-font.min.css" rel="stylesheet" media="all">
    <link rel="stylesheet" href="graphics/css/searchPanes.dataTables.min.css"/>
    <link rel="stylesheet" href="graphics/css/select.dataTables.min.css"/>

    <link rel="stylesheet" type="text/css"  href="../dashboard/DataTable/css/buttons.dataTables.min.css" />
    <link rel="stylesheet" type="text/css"  href="../dashboard/DataTable/css/jquery.dataTables.min.css" />

    <!-- Bootstrap CSS-->
    <link href="vendor/bootstrap-4.1/bootstrap.min.css" rel="stylesheet" media="all">

    <!-- Vendor CSS-->
    <link href="vendor/animsition/animsition.min.css" rel="stylesheet" media="all">
    <link href="vendor/bootstrap-progressbar/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet" media="all">
    <link href="vendor/wow/animate.css" rel="stylesheet" media="all">
    <link href="vendor/css-hamburgers/hamburgers.min.css" rel="stylesheet" media="all">
    <link href="vendor/slick/slick.css" rel="stylesheet" media="all">
    <link href="vendor/select2/select2.min.css" rel="stylesheet" media="all">
    <link href="vendor/perfect-scrollbar/perfect-scrollbar.css" rel="stylesheet" media="all">

    <!-- Main CSS style-->
    <link href="css/theme.css" rel="stylesheet" media="all">

</head>

<!-- Corps du site web -->
<body class="animsition">
    <div class="page-wrapper">
        <!-- HEADER MOBILE :) -->
        <header class="header-mobile d-block d-lg-none">
            <div class="header-mobile__bar">
                <div class="container-fluid">
                    <div class="header-mobile-inner">
                        <a class="logo" href="index.html">
                            <!-- Logo Mcdonald's -->
                            <img src="images/icon/logo.png" alt="CoolAdmin" />
                        </a>
                        <button class="hamburger hamburger--slider" type="button">
                            <span class="hamburger-box">
                                <span class="hamburger-inner"></span>
                            </span>
                        </button>
                    </div>
                </div>
            </div>
            <nav class="navbar-mobile">
                <div class="container-fluid">
                    <ul class="navbar-mobile__list list-unstyled">
                        <li class="has-sub">
                            <a class="js-arrow" href="#">

                            <!-- Éléments à gauche du site web (liste dashboard) -->
                                <i class="fas fa-tachometer-alt"></i>Outils Disponibles</a>
                            <ul class="navbar-mobile-sub__list list-unstyled js-sub-list">
                            <li>
                                <!-- Fonctionnalité : Suivis chiffres d'affaires -->
                                    <a href="suiviCa.php">Suivi CA</a>
                                </li>
                                <li>
                                     <!-- Fonctionnalité :Feuille de caisse -->
                                    <a href="FDC.php">Feuille de caisse</a>
                                </li>
                                <li>
                                     <!-- Fonctionnalité : Produits Mix -->
                                    <a href="PMIX.php">PMIX/MOIS</a>
                                </li>
                                <li>
                                     <!-- Fonctionnalité : Petit Déjeuner -->
                                    <a href="petit-dejeuner.php">Petit Déjeuner/Mois</a>
                                </li>
                            </ul>
                        </li>
                        <!-- <li>
                            <a href="chart.html">
                                <i class="fas fa-chart-bar"></i>Statistiques</a>
                        </li>
                        <li class="has-sub">
                            <a class="js-arrow" href="#">
                                <i class="fas fa-desktop"></i>UI Elements</a>
                            <ul class="navbar-mobile-sub__list list-unstyled js-sub-list">
                                <li>
                                    <a href="button.html">Button</a>
                                </li>
                                <li>
                                    <a href="badge.html">Badges</a>
                                </li>
                                <li>
                                    <a href="tab.html">Tabs</a>
                                </li>
                                <li>
                                    <a href="card.html">Cards</a>
                                </li>
                                <li>
                                    <a href="alert.html">Alerts</a>
                                </li>
                                <li>
                                    <a href="progress-bar.html">Progress Bars</a>
                                </li>
                                <li>
                                    <a href="modal.html">Modals</a>
                                </li>
                                <li>
                                    <a href="switch.html">Switchs</a>
                                </li>
                                <li>
                                    <a href="grid.html">Grids</a>
                                </li>
                                <li>
                                    <a href="fontawesome.html">Fontawesome Icon</a>
                                </li>
                                <li>
                                    <a href="typo.html">Typography</a>
                                </li> -->
                            </ul>
                        </li>
                    </ul>
                </div>
            </nav>

          

        </header>
        <!-- FIN HEADER MOBILE-->

        <!-- MENU SIDEBAR-->
        <aside class="menu-sidebar d-none d-lg-block">
            <div class="logo">
            <a href="suiviCA.php">
                    <img src="images/icon/logo.png" alt="Cool Admin"  width="500" height="500"/>

                    
                </a>
            </div>
            <div class="menu-sidebar__content js-scrollbar1">
                <nav class="navbar-sidebar">
                    <ul class="list-unstyled navbar__list">
                        <li class="active has-sub">
                            <a class="js-arrow" href="#">

    
 <!-- Fonctionnalité : Suivis CA -->
                                <i class="fas fa-tachometer-alt"></i>Outils Disponibles</a>
                            <ul class="list-unstyled navbar__sub-list js-sub-list">
                                <li>
                                    <a href="suiviCa.php">Suivi CA</a>
                                </li>
                                <li>
                                     <!-- Fonctionnalité : Feuille de caisse -->
                                    <a href="FDC.php">Feuille de caisse</a>
                                </li>
                                <li>
                                     <!-- Fonctionnalité : ProduitsMix -->
                                    <a href="PMIX.php">PMIX/MOIS</a>
                                </li>
                                <li>
                                     <!-- Fonctionnalité : Petit déjeuner -->
                                    <a href="petit-dejeuner.php">Petit Déjeuner/Mois</a>
                                </li>
                            </ul>
                        </li>
                        <!-- <li>
                            <a href="chart.html">
                                <i class="fas fa-chart-bar"></i>Statistiques</a>
                        </li>
                        <li class="has-sub">
                            <a class="js-arrow" href="#">
                                <i class="fas fa-desktop"></i>UI Elements</a>
                            <ul class="list-unstyled navbar__sub-list js-sub-list">
                                <li>
                                    <a href="button.html">Button</a>
                                </li>
                                <li>
                                    <a href="badge.html">Badges</a>
                                </li>
                                <li>
                                    <a href="tab.html">Tabs</a>
                                </li>
                                <li>
                                    <a href="card.html">Cards</a>
                                </li>
                                <li>
                                    <a href="alert.html">Alerts</a>
                                </li>
                                <li>
                                    <a href="progress-bar.html">Progress Bars</a>
                                </li>
                                <li>
                                    <a href="modal.html">Modals</a>
                                </li>
                                <li>
                                    <a href="switch.html">Switchs</a>
                                </li>
                                <li>
                                    <a href="grid.html">Grids</a>
                                </li>
                                <li>
                                    <a href="fontawesome.html">Fontawesome Icon</a>
                                </li>
                                <li>
                                    <a href="typo.html">Typography</a>
                                </li> -->
                            </ul>
                        </li>
                    </ul>
                </nav>
            </div>
        </aside>
        <!-- FIN MENU SIDEBAR-->

        <!-- PAGE CONTAINER-->
        <div class="page-container">
            <!-- HEADER BUREAU-PC -->
            <header class="header-desktop">
                <div class="section__content section__content--p30">
                    <div class="container-fluid">
                        <div class="header-wrap">
                            <form class="form-header" action="" method="POST">
                                <input class="au-input au-input--xl" type="text" name="search" placeholder="Search for datas &amp; reports..." />
                                <button class="au-btn--submit" type="submit">
                                    <i class="zmdi zmdi-search"></i>
                                </button>
                            </form>
                            <div class="header-button">
                                <div class="noti-wrap">
                                    <div class="noti__item js-item-menu">
                                        
                                        
                                       
                                    </div>
                                </div>
                                <div class="account-wrap">
                                    <div class="account-item clearfix js-item-menu">
                                        <div class="image">
                                            <img src="images/icon/avatar-01.png" alt="User" />
                                        </div>
                                        <div class="content">
                                            <!-- Choix utilisateur - Déconnexion -->
                                            <a class="js-acc-btn" href="#">Préférence</a>

                                           
                                        </div>
                                        <div class="account-dropdown js-dropdown">
                                            <!-- <div class="info clearfix">
                                                <div class="image">
                                                    <a href="#">
                                                        <img src="images/icon/avatar-01.jpg" alt="John Doe" />
                                                    </a>
                                                </div>
                                                <div class="content">
                                                    <h5 class="name">
                                                        <a href="#">john doe</a>
                                                    </h5>
                                                    <span class="email">johndoe@example.com</span>
                                                </div>
                                            </div> -->
                                            <div class="account-dropdown__body">
                                                <!-- <div class="account-dropdown__item">
                                                    <a href="#">
                                                        <i class="zmdi zmdi-account"></i>Infos Compte</a>
                                                </div>
                                                <div class="account-dropdown__item">
                                                    <a href="#">
                                                        <i class="zmdi zmdi-settings"></i>Setting</a>
                                                </div>
                                                <div class="account-dropdown__item">
                                                    <a href="#">
                                                        <i class="zmdi zmdi-money-box"></i>Billing</a>
                                                </div>
                                            </div> -->
                                            <div class="account-dropdown__footer">
                                                <a href="../login/logout.php">
                                                    <i class="zmdi zmdi-power"></i>Déconnexion</a>

                                                    
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </header>
            <!-- FIN HEADER BUREAU-->

            <!-- MAIN CONTENT-->
            <div class="main-content">
                <div class="section__content section__content--p30">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="overview-wrap">
                                    <h2 class="title-1">Bienvenue : FEUILLE DE CAISSES</h2>

                                   
                                    <!-- <button class="au-btn au-btn-icon au-btn--blue">
                                        <i class="zmdi zmdi-plus"></i>add item</button> -->
                                </div>
                            </div>
                        </div>
                        <div class="row m-t-25">
                            <div class="col-sm-6 col-lg-3">
                                <div class="overview-item overview-item--c1">
                                    <div class="overview__inner">
                                        <div class="overview-box clearfix">
                                            <div class="icon">
                                                <i class="zmdi zmdi-account-o"></i>
                                            </div>
                                            <div class="text">
                                            <h2>PMIX/MOIS</h2>
                                                <a href="PMIX.php" style="color:white">Cliquez-ici</a>
                                            </div>
                                        </div>
                                        <div class="overview-chart">
                                            <canvas id="widgetChart1"></canvas>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6 col-lg-3">
                                <div class="overview-item overview-item--c2">
                                    <div class="overview__inner">
                                        <div class="overview-box clearfix">
                                            <div class="icon">
                                                <i class="zmdi zmdi-shopping-cart"></i>
                                            </div>
                                            <div class="text">
                                            <h2>FDC</h2>
                                                <a href="FDC.php" style="color:white">Cliquez-ici</a>
                                            </div>
                                        </div>
                                        <div class="overview-chart">
                                            <canvas id="widgetChart2"></canvas>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6 col-lg-3">
                                <div class="overview-item overview-item--c3">
                                    <div class="overview__inner">
                                        <div class="overview-box clearfix">
                                            <div class="icon">
                                                <i class="zmdi zmdi-calendar-note"></i>
                                            </div>
                                            <div class="text">
                                            <h2>Petit Déj/Mois</h2>
                                                <a href="petit-dejeuner.php" style="color:white">Cliquez-ici</a>
                                            </div>
                                        </div>
                                        <div class="overview-chart">
                                            <canvas id="widgetChart3"></canvas>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6 col-lg-3">
                                <div class="overview-item overview-item--c4">
                                    <div class="overview__inner">
                                        <div class="overview-box clearfix">
                                            <div class="icon">
                                                <i class="zmdi zmdi-money"></i>
                                            </div>
                                            <div class="text">
                                            <h2>Suivi CA</h2>
                                                <a href="suiviCA.php" style="color:white">Cliquez-ici</a>
                                            </div>
                                        </div>
                                        <div class="overview-chart">
                                            <canvas id="widgetChart4"></canvas>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="au-card recent-report">
                                    <div class="au-card-inner">
                                        <h3 class="title-2">recent reports</h3>
                                        <div class="chart-info">
                                            <div class="chart-info__left">
                                                <div class="chart-note">
                                                    <span class="dot dot--blue"></span>
                                                    <span>products</span>
                                                </div>
                                                <div class="chart-note mr-0">
                                                    <span class="dot dot--green"></span>
                                                    <span>services</span>
                                                </div>
                                            </div>
                                            <div class="chart-info__right">
                                                <div class="chart-statis">
                                                    <span class="index incre">
                                                        <i class="zmdi zmdi-long-arrow-up"></i>25%</span>
                                                    <span class="label">products</span>
                                                </div>
                                                <div class="chart-statis mr-0">
                                                    <span class="index decre">
                                                        <i class="zmdi zmdi-long-arrow-down"></i>10%</span>
                                                    <span class="label">services</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="recent-report__chart">
                                            <canvas id="recent-rep-chart"></canvas>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="au-card chart-percent-card">
                                    <div class="au-card-inner">
                                        <h3 class="title-2 tm-b-5">char by %</h3>
                                        <div class="row no-gutters">
                                            <div class="col-xl-6">
                                                <div class="chart-note-wrap">
                                                    <div class="chart-note mr-0 d-block">
                                                        <span class="dot dot--blue"></span>
                                                        <span>products</span>
                                                    </div>
                                                    <div class="chart-note mr-0 d-block">
                                                        <span class="dot dot--red"></span>
                                                        <span>services</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-6">
                                                <div class="percent-chart">
                                                    <canvas id="percent-chart"></canvas>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-9">
                                <h2 class="title-1 m-b-25">FEUILLE DE CAISSES</h2>
                              
 
        
        
        <center>




  <link rel="stylesheet" href="css/jquery-ui.css">
 <!-- <link rel="stylesheet" href="/resources/demos/style.css">-->
  <script src="js/jquery-ui.js"></script>
  
 <p><?php 
// echo date('d-m-Y', strtotime('-1 year'));
?></p>

<!-- Selection d'une période de Date -->
<p>Sélectionner une Date : </p>
<br/>
<p>Format : (Année-mois-Jour) </p>





<!-- Date Picker + Fonction JS lors de la selection de date -->
<input type="date" id='dateTake'/>

&nbsp;
<input type="date" id='dateTake2' onchange='initialise_datatable()'/>

<input type="hidden" id="submittedDate" />



<!-- <div id ="container" class="container" style="width:100%; height:400px;"> -->


</div>






</center>
              
              </div>
<br/><br/>
              <center>
               <!--<label for="start">Date:</label>

                <input type="date" id="start" name="trip-start"
                       value="2018-07-22"
                       min="2018-01-01" max="2018-12-31">-->


                       


                </center>
                
              <center>
              <div class="card-body">
                <div class="table-responsive">
                  <div class="limiter"></div>

             


                  <!--- Tableau concernant les données : Feuille de caisse--> 
                   
                      <div class="wrap-table100">
                        
 

<br/><br/>
<div id="test">
<table id='empTable' class='display dataTable'>
                <thead>
                <tr>
                <th>Code Restos</th>
                    <th>Nom Restos</th>
                    <th>Libélée</th>
                    <!--<th>Ventes N-1</th>-->
				          	<th>Compté</th>
                </tr>
                </thead>
                <tfoot>
        <tr>
        <th>Code Restos</th>
                    <th>Nom Restos</th>
                    <th>Libélée</th>
                    <!--<th>Ventes N-1</th>-->
				          	<th>Compté</th>
        </tr>
    </tfoot>
                
            </table>
        </div>

  <!-- Script JS+AJAX recup données via les fichiers de types PHP  -->
        <script>

        


function initialise_datatable() {
  var date = document.getElementById('dateTake');
  var annee = date.value.substr(0,4);
  var mois = date.value.substr(5,2);
  var jour = date.value.substr(8,2);
  var dateF = annee+mois+jour;

  //Récupération de la date puis hashage en yyyy-mois-jour de la deuxième Date

  var date2 = document.getElementById('dateTake2');
  var annee2 = date2.value.substr(0,4);
  var mois2 = date2.value.substr(5,2);
  var jour2 = date2.value.substr(8,2);
  var dateF2 = annee2+mois2+jour2;

   //Petite condition dans le but d'obliger l'utilisateur de compléter la première date picker avant d'afficher les données
 
  if( !dateF ){

alert(" Tu as oublié de remplir la première date.  🙂.    ")
return false;
}

//DataTable : Script en Ajax communicant avec le fichier "SuiviCA.php" afin d'alimenter le tableau

 if( $.fn.dataTable.isDataTable( '#empTable' )){
     //ID du DtaTable
   table = $('#empTable').DataTable();
      //Destroy afin d'écraser les données/ réinitiliaser les données du tableau en cas de renouvellement de Date

  table.destroy();
  $('#empTable').DataTable( {
      
      'processing': true,
      'retrieve': true,
       'serverSide': true,
       'searching' : false,
        'serverMethod': 'post',
         'ajax': {
          'url':'configAjax/fdc.php',
          'data':{
            'dateF': dateF,
            'dateF2': dateF2,
          }
                },
                //Afficher le nombre de résultats
                dom: 'lBfrtip',
        buttons: [
    //buttons d'export en CSV,EXCEl, PDF, Impression et copier les données présentes dans le tableau

            'copy', 'csv', 'excel', 'pdf', 'print'
        ],
        'columns': [
            { data: 'stpsto' },
            { data: 'stpnam' },
            { data: 'dptcur' },
            { data: 'amount' },
                  
                ]
            })
 }else{
     
  $('#empTable').DataTable( {
      
      'processing': true,
      'retrieve': true,
       'serverSide': true,
       'searching' : false,
        'serverMethod': 'post',
         'ajax': {
          'url':'configAjax/fdc.php',
          'data':{
            'dateF': dateF,
            'dateF2': dateF2,
          }
                },
                dom: 'lBfrtip',
        buttons: [
  
            'copy', 'csv', 'excel', 'pdf', 'print'
        ],
        'columns': [
            { data: 'stpsto' },
            { data: 'stpnam' },
            { data: 'dptcur' },
            { data: 'amount' },
                  
                ]
            })
          }
 
}




        </script>



                      
                      </div>
                    </div>
                 <!-- </div>-->
                </div>
             
   
    


 

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="copyright">
                                    <p>Copyright © 2021.  By J-Damien VALERE</a>.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- FIN MAIN CONTENT-->
            <!-- FIN PAGE CONTAINER-->
        </div>

    </div>

    <!-- Jquery JS-->
    
    <!-- Bootstrap JS-->
    <script src="../dashboard/DataTable/js/jquery-3.5.1.js"></script>
              <script src="../dashboard/DataTable/js/jquery.dataTables.min.js"></script>
              <script src="../dashboard/DataTable/js/dataTables.buttons.min.js"></script>
              <script src="../dashboard/DataTable/js/jszip.min.js"></script>
              <script src="../dashboard/DataTable/js/pdfmake.min.js"></script>
              <script src="../dashboard/DataTable/js/vfs_fonts.js"></script>
              <script src="../dashboard/DataTable/js/buttons.html5.min.js"></script>
              <script src="../dashboard/DataTable/js/buttons.print.min.js"></script>
    <script src="vendor/bootstrap-4.1/popper.min.js"></script>
    <script src="vendor/bootstrap-4.1/bootstrap.min.js"></script>
    <!-- Vendor JS       -->
    <script src="vendor/slick/slick.min.js">
    </script>
    <script src="vendor/wow/wow.min.js"></script>
    <script src="vendor/animsition/animsition.min.js"></script>
    <script src="vendor/bootstrap-progressbar/bootstrap-progressbar.min.js">
    </script>
    <script src="vendor/counter-up/jquery.waypoints.min.js"></script>
    <script src="vendor/counter-up/jquery.counterup.min.js">
    </script>
    <script src="vendor/circle-progress/circle-progress.min.js"></script>
    <script src="vendor/perfect-scrollbar/perfect-scrollbar.js"></script>
    <script src="vendor/chartjs/Chart.bundle.min.js"></script>
    <script src="vendor/select2/select2.min.js">
    </script>

    <!-- Main JS-->
    <script src="js/main.js"></script>

</body>

</html>
<!-- end document-->
