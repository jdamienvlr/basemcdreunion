<?php

include('database_connection.php');

$pmxmic = '';
$query = "SELECT DISTINCT pmxmic FROM cshpmxp1_1960 ORDER BY pmxmic ASC";
$statement = $connect->prepare($query);
$statement->execute();
$result = $statement->fetchAll();
foreach($result as $row)
{
 $pmxmic .= '<option value="'.$row['pmxmic'].'">'.$row['pmxmic'].'</option>';
}



?>

<html>
 <head>
  <title>Custom Search in jQuery Datatables using PHP Ajax</title>
 
  <link rel="stylesheet" href="css/bootstrap.min.css" />
  <link rel="stylesheet" href="css/dataTables.bootstrap.min.css" />


  <link rel="stylesheet" href="css/buttons.dataTables.min.css" />
  <link rel="stylesheet" href="css/jquery.dataTables.min.css" /> 


  <script src="js/jquery.min.js"></script>
  <script src="js/jquery.dataTables.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script src="js/dataTables.bootstrap.min.js"></script>  

  <script src="js/dataTables.buttons.min.js"></script>
  <script src="js/jszip.min.js"></script>
  <script src="js/pdfmake.min.js"></script>
  <script src="js/vfs_fonts.js"></script>
  <script src="js/buttons.html5.min.js"></script>
  <script src="js/buttons.print.min.js"></script>

  <!-- <script src="js/jquery-3.5.1.js"></script>
  <script src="js/dataTables.buttons.min.js"></script>
  <script src="js/jszip.min.js"></script>
  <script src="js/pdfmake.min.js"></script>
  <script src="js/vfs_fonts.js"></script>
  <script src="js/buttons.html5.min.js"></script>
  <script src="js/buttons.print.min.js"></script> -->
  
 </head>
 <body>
  <div class="container box">
   <h3 align="center">Produits Mix</h3>
   <br />
   <div class="row">
    <div class="col-md-4"></div>
    <div class="col-md-4">
     <div class="form-group">
      <select name="filter_stpsto" id="filter_stpsto" class="form-control" required>
       <option value="">Selection du restaurant</option>
       <option value="1960">Sainte-Marie</option>
       <option value="1961">Le Port</option>
       <option value="1962">St Denis</option>
       <option value="1963">St Benoit</option>
       <option value="1964">St Pierre</option>
       <option value="1967">St Pierre 400</option>
       <option value="1968">La Possession</option>
       <option value="1969">St Louis</option>
       <option value="1970">Eperon</option>
       <option value="1971">Continent</option>
       <option value="1972">St Joseph</option>
      </select>
     </div>
     <div class="form-group">
      <select name="filter_pmxmic" id="filter_pmxmic" class="form-control" required>
       <option value="">Selection du Produit</option>
       <?php echo $pmxmic; ?>
      </select>
     </div>
     <div class="form-group" align="center">
      <button type="button" name="filter" id="filter" class="btn btn-info">Filtrer !</button>
     </div>
    </div>
    <div class="col-md-4"></div>
   </div>
   <div class="table-responsive">
    <table id="customer_data" class="table table-bordered table-striped">
     <thead>
      <tr>
       <th width="20%">Code Restos</th>
       <th width="10%">Nom restos</th>
       <th width="25%">Code PMIX</th>
       <th width="15%">Nom Pmix</th>
       <th width="15%">CA BRUT SP</th>
       <th width="15%">CA BRUT EMP</th>
       <th width="25%">CA NET SP</th>
       <th width="15%">CA NET EMP</th>
       <th width="15%">QT SP</th>
       <th width="15%">QT EMP</th>
      </tr>
     </thead>
    </table>
    <br />
    <br />
    <br />
   </div>
  </div>
 </body>
</html>

<script type="text/javascript" language="javascript" >
 $(document).ready(function(){
  
  fill_datatable();
  
  function fill_datatable(filter_stpsto = '', filter_pmxmic = '')
  {
   var dataTable = $('#customer_data').DataTable({
    "processing" : true,
    "serverSide" : true,
    "order" : [],
    "searching" : true,
    "ajax" : {
     url:"fetch.php",
     type:"POST",
     data:{
      filter_stpsto:filter_stpsto, filter_pmxmic:filter_pmxmic
     }
    },
                dom: 'lBfrtip',
        buttons: [
  
            'copy', 'csv', 'excel', 'pdf', 'print'
        ],
   });
  }
  
  $('#filter').click(function(){
   var filter_stpsto = $('#filter_stpsto').val();
   var filter_pmxmic = $('#filter_pmxmic').val();
   if(filter_stpsto != '' && filter_pmxmic != '')
   {
    $('#customer_data').DataTable().destroy();
    fill_datatable(filter_stpsto, filter_pmxmic);
   }
   else
   {
    alert('Select Both filter option');
    $('#customer_data').DataTable().destroy();
    fill_datatable();
   }
  });
  
  
 });
 
</script>